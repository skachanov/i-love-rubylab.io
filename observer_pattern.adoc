== Observer Pattern

Object Oriented Programming is modeled after real world. Here objects need to communicate with one another, and other objects need to react when ones object state changes. Lets say that you have a situation where a object's state change needs to be propagated to n-number of other objects, those other objects are called observers. How to write a neat and tidy code to notify observers when something changes? Welcome to Observer Pattern.

Take a look at link:code/design_patterns/observer.rb[observer.rb], the code listing is shown below

[source, ruby, linenums]
----
include::code/design_patterns/observer.rb[]
----

In the code above take a look at these lines

[source, ruby]
----
vel = Person.new "Vel"
vel.observers << Person.new("Murugan")
vel.observers << Person.new("Swami")
----

So from the code above we know that there is a person called Vel who is observed by Murugan and Swami. Just imagine a social network where Vel is followed by Murugan and Swami. So we have an attribute called `observers` in `Person`, which is nothing but an array that can take in as many observers as possible.

If you look at link:code/design_patterns/observer.rb[observer.rb], you will notice that it has been accomplished in these lines

[source, ruby]
----
class Person
  attr_accessor :name, :status, :observers

  def initialize name
    @name = name
    @observers =[]
  end

  ...
end
----

Next look at this line

[source, ruby]
----
vel.set_status "Hello All!"
----

In it we set the status of Vel. When we run the program we get the following output:

[source, bash]
----
Vel: Hello All! - notified to Murugan
Vel: Hello All! - notified to Swami
----

So as you can see that the observers have been notified about Vel's new status. How this was accomplished? If you look at link:code/design_patterns/observer.rb[observer.rb], in the method `set_status` we would have called the method `notify_observers`, in it the magic happens.

Take a look at `notify_observers` method

[source, ruby]
----
class Person
  ...

  def notify_observers
    for observer in observers<1>
      observer.notify self<2>
    end
  end

  ...

end
----

In it the following happens

<1> We iterate through each observer.
<2> We call `notify` method in the observer and pass the changed object.

Since the observers are all of the type `Person`, we have written the `notify` method in the same class as, take a look at the code below. In it

[source, ruby]
----
class Person
  ...

  def notify person<1>
    puts "#{person.name}: #{person.status} - notified to #{@name}"<2>
  end
end
----

<1> `notify` receives the changed object, in this case as `person`.
<2> It does something with the changed object.

So this is how observer pattern works. We have way to store observers, we have a method to notify observers which is called when notification needs to be made and finally we have a method is observer to receive the changed object. The observer can do what it may wish with the changed object.

NOTE: Actually when you are using Ruby, observer pattern is baked right into its standard library. Check this out https://ruby-doc.org/stdlib-2.7.0/libdoc/observer/rdoc/Observable.html

