=== Using Text Editor

Till now you have keyed in small programs into your irb, when you are developing large software you can't expect the end user or your clients to keep keying in into the console the statements you have developed for him / her, instead you will be handing over a typed Ruby program which they can run it to accomplish certain task. Let's see how to use a text editor to write programs.
Earlier in  Installing IDE section I have typed about how to install a simple Integrated Development Environment (IDE) called Geany (https://geany.org). If you are using Ubuntu, press super key, type in Geany,  click on the Geany icon and you will get it.

image::geany.png[]

You can use other IDE's too, if want other IDE, refer to their documentation for installation instructions. In the IDE type the following program

[source, ruby]
----
include::code/hello_world.rb[]
----

Now save the file as hello_world.rb in a directory, note that Ruby files ends with .rb (dot rb) extension. Launch your terminal / console, migrate to the directory where program is stored and type the following in it

[source, ruby]
----
$ ruby hello_world.rb
----
and here's how you will get the output.

[source, ruby]
----
Hello World!
This time I used text editor
----

Wonderful! You have learned to program with a text editor, you are getting professional aye!

