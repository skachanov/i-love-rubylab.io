age = 27

if age in ..17
  then puts "You are a child"
end

if age in 18..
  then puts "You are an adult"
end

if age in 21..30
  then puts "You can join the Army"
end

if age in 31..
  then puts "You can't join the Army"
end

if age in ..59
  then puts "You are still young"
end

if age in 60..
  then puts "You are old"
end
