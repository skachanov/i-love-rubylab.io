# anonymous_class.rb

person = Class.new do
  def say_hi
    'Hi'
  end
end.new

puts person.say_hi
puts person.class
