# argument_forwarding_4.rb

def rest_of_arguments *args
  puts "In rest_of_arguments"
  puts args
end

def last_argument_and_forward_others(..., a)
  puts "In last_argument_and_forward_others"
  puts a
  rest_of_arguments(...)
end

last_argument_and_forward_others(1, 2, 3, 4, 5)
