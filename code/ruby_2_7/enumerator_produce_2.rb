# easy Fibonacci
p Enumerator.produce([0, 1]) { |f0, f1| [f1, f0 + f1] }.take(10).map(&:first)
