p [1, 2, 3].select { |x| x.odd? }.map { |x| x.to_s }
p [1, 2, 3].map { |x| x.to_s if x.odd? }.compact
p [1, 2, 3].each_with_object([]) { |x, m| m.push(x.to_s) if x.odd? }

# now let's see Enumerable#filter_map

p [1, 2, 3].filter_map {|x| x.odd? ? x.to_s : nil } #=> ["1", "3"]
