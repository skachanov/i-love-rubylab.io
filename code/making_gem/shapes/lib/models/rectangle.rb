class Rectangle
  attr_accessor :length, :height

  def area
    length * height
  end

  def perimeter
    2 * (length + height)
  end
end