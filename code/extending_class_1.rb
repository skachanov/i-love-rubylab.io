# extending_class_1.rb

class Integer
  def minute
    self * 60
  end

  def hour
    minute * 60
  end

  def day
    hour * 24
  end

  def week
    day * 7
  end
end

puts Time.now + 2.week
