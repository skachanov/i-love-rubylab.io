== Ruby

Ruby is an easy to learn programming language, it was invented by a guy named Matzfootnote:[https://twitter.com/yukihiro_matz , https://en.wikipedia.org/wiki/Yukihiro_Matsumoto] in Japan. Ruby is a freefootnote:[Free here does not mean zero cost. Visit http://fsf.org to know more] software and can be used by any one for zero cost. Ruby's popularity was initially confined to Japan, later it slowly trickled out to rest of the world. Things changed with the emergence of Ruby on Railsfootnote:[http://rubyonrails.org] which is a popular web-development framework written with Ruby.

I was thrilled when I started to program in Ruby. One of my first application was a student ranking software for my mom who was a teacher. I was able to write the console based application in just 32 lines!!! This opened my eyes and made me realize the power of Ruby. The language was simple, easy to learn and nearly perfect. Currently I am a professional Ruby on Rails programmer.

This book is written for GNU/Linux (Debian distro) users, that's because I think GNU/Linux will conquer desktops of programmers in near future. Almost all who have Debian GNU/Linux based distro should feel at home while trying to learn Ruby using this book. If you are using other operating systems like Solaris, OSX or Windows please contact your Operating System help channels to learn how to install or get started with Ruby. You can also visit http://ruby-lang.org to learn how get started with Ruby.

