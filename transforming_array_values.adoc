=== Transforming array values

Ruby comes with very powerful array operations, we would see how we can transform arrays in easy to use commands.

==== Collect

Fire up your pry or irb and type the following

[source, ruby]
----
>> array = [1, 2, 3]
=> [1, 2, 3]
>> array.collect{ |element| element * element }
=> [1, 4, 9]
----

In the above code we have declared a variable `array` of type `Array`, in the next statement we are calling a method called `collect` on it and we pass a this block of code `{ |element| element * element }` . Now lets see what it does.

When `collect` is called, first, a return array is created. In the block we see a thing called `|element|`, now the collect algorithm works like this. First this array has values 1, 2 and 3, so this block runs three times. During the first run, 1 gets loaded into `element`, now in the block we have specified `element * element`, so `1 * 1` is `1`, and it gets pushed into the return array. Now the return array is `[1]`.

Now it takes the second value, that is 2 and the same process occurs and now the return array is `[1, 4]`, similarly the return array finally becomes `[1, 4, 9]` and is returned out.

`collect` does not modify the object upon which its called, so if you look what `array` is, it will still be the same as shown below

[source, ruby]
----
>> array
=> [1, 2, 3]
----

You can check it in irb. If you want collect to modify the object that its been called upon, use it with a bang as shown below

[source, ruby]
----
>> array.collect!{ |element| element * element }
=> [1, 4, 9]
>> array
=> [1, 4, 9]
----

==== keep_if

Lets say that we want to have elements in an array that match certain condition and want to take out others, we can use `keep_if` function as shown below

[source, uby]
----
>> array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
=> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
>> array.keep_if{ |element| element % 2 == 0}
=> [2, 4, 6, 8, 10]
----

We have an array of ten elements above and say we want to keep elements that are even or divisible by 2, so we write as statement as shown

[source, ruby]
----
array.keep_if{ |element| element % 2 == 0}
----

To the `keep_if` we pass a block. In it, each and every value in the array is captured in `|element|` and there is condition written  `element % 2 == 0` , if the condition is true, the element is kept in the array, or its thrown out.  The `keep_if` modifies the array upon which its called, unlike collect which returns the new array. So if you check array after running `keep_if` on it, its content would have changed.

==== delete_if

As apposed to `keep_if` , `delete_if` does the exact opposite, below is shown what happens if you run `delete_if` on an Array object.

[source, ruby]
----
>> array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
=> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
>> array.delete_if{ |element| element % 2 == 0}
=> [1, 3, 5, 7, 9]
>> array
=> [1, 3, 5, 7, 9]
----

=== Read the ruby doc

Definitely this book isn’t comprehensive enough to cover all of Ruby Array library. To know more read the Ruby docs here https://ruby-doc.org/core-3.0.3/Array.html

