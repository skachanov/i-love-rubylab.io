=== Strings

Till now we have seen about numbers, now lets see something about text. In computers text are called as stringfootnote:[Possibly because they are string of characters]. OK lets see about strings in Ruby. Lets start with an hello world. In your irb type hello world as shown

[source, ruby]
----
>> "hello world"
=> "hello world"
----

As a response you get an `“hello world”`. In short, string is any stuff that's surrounded by `“` or by `'`

Now lets try the above example by surrounding the above hello world with single quotes

[source, ruby]
----
>> 'hello world'
=> "hello world"
----

Well you do get the same response. So whats the difference between single and double quotes? Take a look at the following example

[source, ruby]
----
>> time_now = Time.new # Get the current time into a variable
=> Fri Jan 15 16:43:31 +0530 2010
>> "Hello world, the time is now #{time_now}"
=> "Hello world, the time is now Fri Jan 15 16:43:31 +0530 2010"
>> 'Hello world, the time is now #{time_now}'
=> "Hello world, the time is now \#{time_now}"
----

At first we declare a variable called `time_now` and store the current time into it. The current time in Ruby is got by `Time.new` command. Now we have a variable and we can embed it into a string by putting it like `#{put_your_variable_here}`. So we want to tell the world the time now is something, so we give a command as shown

[source, ruby]
----
>> "Hello world, the time is now #{time_now}"
=> "Hello world, the time is now Fri Jan 15 16:43:31 +0530 2010"
----

and we get a proper result. Note that you have enclosed the string with a double quotes. Now lets try the same thing with single quotes

[source, ruby]
----
>> 'Hello world, the time is now #{time_now}'
=> "Hello world, the time is now \#{time_now}"
----

We see that in this case the world is not able to see what time it is, rather its able to see a ugly string as shown

[source, ruby]
----
"Hello world, the time is now \#{time_now}"
----

What ever that's put between single quotes gets printed as it is. You might ask why # is printed as `\#`, well we will see it in escape sequence soon.

include::string_functions.adoc[]

include::escape_sequence.adoc[]

